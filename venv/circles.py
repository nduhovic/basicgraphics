import arcade

WIDTH = 1200
HEIGTH = 800
COLUMNS= 60
ROWS= 20
LEFT_MARGIN = 50
BOTTOM_MARGIN = 50
CELL_WIDTH = (WIDTH-LEFT_MARGIN)/COLUMNS
CELL_HEIGTH = (HEIGTH-BOTTOM_MARGIN)/ROWS

def draw_circle(x,y):
    arcade.draw_circle_filled(x, y, 15, arcade.color.RED)

def draw_grid():
    for row in range(ROWS):
        arcade.draw_text(str(row), 20, row * CELL_HEIGTH + BOTTOM_MARGIN, arcade.color.BLACK, 12)
        # Loop for each column
        for column in range(COLUMNS):
            # Calculate our location

            x = column * CELL_WIDTH + LEFT_MARGIN
            y = row * CELL_HEIGTH + BOTTOM_MARGIN
            x_end = column * CELL_WIDTH + LEFT_MARGIN +CELL_WIDTH
            y_end = row * CELL_HEIGTH + BOTTOM_MARGIN + CELL_HEIGTH

            # Draw the item
            arcade.draw_line(x, y, x, y_end, arcade.color.DARK_GRAY)
            arcade.draw_line(x, y, x_end, y, arcade.color.DARK_GRAY)
            if row == 0:
                arcade.draw_text(str(column), column * CELL_WIDTH + LEFT_MARGIN, 10, arcade.color.BLACK, 12)

class MyGame(arcade.Window):

    def __init__(self, width, height, title):

        # Call the parent class's init function
        super().__init__(width, height, title)
        arcade.set_background_color(arcade.color.LIGHT_GRAY)


    def on_draw(self):
        arcade.start_render()
        draw_grid()
        draw_circle(155,165)
        draw_circle(100, 75)
        draw_circle(600,300)





def main():
    window = MyGame(WIDTH, HEIGTH, "circles")
    arcade.run()




if __name__ == "__main__":
    main()